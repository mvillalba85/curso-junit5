package org.mvillalba.junit5app.ejemplos.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Banco {
    private String Nombre;
    private List<Cuenta> cuentas;

/*    public Banco(String nombre) {
        Nombre = nombre;
    }

    public Banco() {

    }*/

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public List<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(List<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public void addCuentas(Cuenta cuenta){
        if(this.cuentas == null){
            this.cuentas = new ArrayList<>();
        }
        cuentas.add(cuenta);
        cuenta.setBanco(this);
    }


    public void transferencia(Cuenta transfiere, Cuenta recibe, BigDecimal montoTransferencia){
        transfiere.debito(montoTransferencia);
        recibe.credito(montoTransferencia);
    }
}
