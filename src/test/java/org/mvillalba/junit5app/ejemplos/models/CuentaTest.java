package org.mvillalba.junit5app.ejemplos.models;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mvillalba.junit5app.ejemplos.exeptions.DineroInsuficienteException;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

class CuentaTest {
    Cuenta cuenta;

    private TestInfo testInfo;
    private TestReporter testReporter;

    @BeforeAll
    static void beforeAll() {
        System.out.println("Iniciando el test");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("Finalizando el test");
    }

    @BeforeEach
    void initMetodoTest(TestInfo testInfo, TestReporter testReporter) {
        this.cuenta = new Cuenta("MarianoV", new BigDecimal("1000.12345"));
        this.testInfo = testInfo;
        this.testReporter = testReporter;

        System.out.println("Iniciando el método");

        //Uso del componente testInfo
        System.out.println("ejecutando: " + testInfo.getDisplayName() + " "
                + testInfo.getTestMethod().orElse(null).getName()
                + " con las etiquetas " + testInfo.getTags());


        //Uso del componente testReporter
        testReporter.publishEntry("ejecutando: " + testInfo.getDisplayName() + " "
                + testInfo.getTestMethod().orElse(null).getName()
                + " con las etiquetas " + testInfo.getTags());
    }

    @AfterEach
    void tearDown() {
        System.out.println("Finalizando el metodo de prueba");
    }

    @Tag("cuenta")
    @Nested
    @DisplayName("Probando atributos de la cuenta corriente")
    class CuentaTestNombreSaldo{
        @Test
        @DisplayName("el nombre!")
        void testNombreCuenta() {
            testReporter.publishEntry(testInfo.getTags().toString());
            if(testInfo.getTags().contains("cuenta")){
                testReporter.publishEntry("Contiene la etiqueta: " + testInfo.getTags());
            }
            //cuenta.setPersona("Mariano");
            String esperado = "MarianoV";
            String real = cuenta.getPersona();
            assertNotNull(real, "la cuenta no puede ser nula");
            assertEquals(esperado, real, "El nombre esperado no es el que se esperaba");
            assertTrue(real.equals("MarianoV"));
        }

        @Test
        @DisplayName("El saldo, que no sea null, mayor que cero, y que se cumpla el valor esperado")
        void testSaldoCuenta() {
            assertNotNull(cuenta.getSaldo());
            assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
            assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @Test
        @DisplayName("Testeando referencias que sean iguales con el método equals")
        void testReferenciaCuenta() {
            Cuenta cuenta = new Cuenta("Mariano", new BigDecimal("1000.12345"));
            Cuenta cuenta2 = new Cuenta("Mariano", new BigDecimal("1000.12345"));

            //assertNotEquals(cuenta2, cuenta);
            assertEquals(cuenta2, cuenta);
        }
    }

    @Nested
    class CuentaOperacionesTest{
        @Tag("cuenta")
        @Test
        void testDebitoCuenta() {
            System.out.println("Saldo antes del debito: " + cuenta.getSaldo());
            cuenta.debito(new BigDecimal(100));
            System.out.println("Saldo de la cuenta: " + cuenta.getSaldo());
            assertNotNull(cuenta.getSaldo());
            assertEquals(900, cuenta.getSaldo().intValue());
            assertEquals("900.12345", cuenta.getSaldo().toPlainString());
        }

        @Tag("cuenta")
        @Test
        void testCreditoCuenta() {
            cuenta.credito(new BigDecimal(100));
            assertNotNull(cuenta.getSaldo());
            assertEquals(1100, cuenta.getSaldo().intValue());
            assertEquals("1100.12345", cuenta.getSaldo().toPlainString());
        }

        @Tag("cuenta")
        @Tag("banco")
        @Test
        void testTranferirDineroCuentas() {
            Cuenta cuentaMariano = new Cuenta("Mariano", new BigDecimal("2500"));
            Cuenta cuentaRocio = new Cuenta("Rocio", new BigDecimal("1500.8989"));

            Banco banco = new Banco();
            banco.setNombre("Banco Nacion");
            banco.transferencia(cuentaMariano, cuentaRocio, new BigDecimal(500));
            assertEquals("2000.8989", cuentaRocio.getSaldo().toPlainString());
            assertEquals("2000", cuentaMariano.getSaldo().toPlainString());
        }
    }

    @Tag("cuenta")
    @Tag("error")
    @Test
    void testDineroInsuficienteExceptionCuenta() {
        Exception exception = assertThrows(DineroInsuficienteException.class, ()-> {
            cuenta.debito(new BigDecimal(1500));
        });
        String actual = exception.getMessage();
        String esperado = "Dinero Insuficiente";
        assertEquals(esperado, actual);

    }


    @Tag("cuenta")
    @Tag("banco")
    @Test
    //@Disabled //Anotacion que deshabilita la prueba del metodo en cuestión
    @DisplayName("Se prueban las relaciones entre el Banco y las Cuentas, utilizando el assertAll")
    void testRelacionBancoCuentas() {
        //fail(); //Método de Junit que provoca que la prueba falle.

        Cuenta cuentaMariano = new Cuenta("Mariano", new BigDecimal("2500"));
        Cuenta cuentaRocio = new Cuenta("Rocio", new BigDecimal("1500.8989"));

        Banco banco = new Banco();
        banco.setNombre("Banco Nacion");
        banco.addCuentas(cuentaMariano);
        banco.addCuentas(cuentaRocio);


        banco.transferencia(cuentaMariano, cuentaRocio, new BigDecimal(500));

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Aqui se ejecutan los assert por separados. Cuando uno no se cumple los siguientes ya no se ejecutan.
        assertEquals("2000.8989", cuentaRocio.getSaldo().toPlainString());
        assertEquals("2000", cuentaMariano.getSaldo().toPlainString());

        //Test numero de cuentas en el banco(relacion de banco con las cuentas)
        assertEquals(2, banco.getCuentas().size());

        //Test para probar relacion de cuentas con el banco
        assertEquals("Banco Nacion", cuentaMariano.getBanco().getNombre());

        //Test para validar que una cuenta del banco pertenezca una cierta persona
        assertEquals("Mariano", banco.getCuentas().stream()
                .filter(cuenta -> cuenta.getPersona().equals("Mariano"))
                .findFirst().get().getPersona());

        //Test para validar con True la presencia de la cuenta según el nombre
        assertTrue(banco.getCuentas().stream().anyMatch(cuenta -> cuenta.getPersona().equals("Rocio")));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Uso del AssertAll para que se ejecunten todos los assert contenidos  dentro
        assertAll(()-> assertEquals("2000.8989", cuentaRocio.getSaldo().toPlainString(),
                        ()-> "El valor del saldo de la cuentaRocio no es el esperado") ,
                ()-> assertEquals("2000", cuentaMariano.getSaldo().toPlainString(),
                        ()-> "El valor del saldo de la cuentaMariano no es el esperado"),
                ()-> assertEquals(2, banco.getCuentas().size(),
                        ()-> "El banco no tiene las cuentas esperadas"), //Test numero de cuentas en el banco(relacion de banco con las cuentas)
                ()-> assertEquals("Banco Nacion", cuentaMariano.getBanco().getNombre()), //Test para probar relacion de cuentas con el banco
                ()-> assertEquals("Mariano", banco.getCuentas().stream() //Test para validar que una cuenta del banco pertenezca a una cierta persona
                        .filter(cuenta -> cuenta.getPersona().equals("Mariano"))
                        .findFirst().get().getPersona()),
                ()-> assertTrue(banco.getCuentas().stream().anyMatch(cuenta -> cuenta.getPersona().equals("Rocio")))); //Test para validar que una cuenta del banco pertenezca a una cierta persona
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    }

    @Nested
    class SistemaOperativoTest{
        @Test
        @EnabledOnOs(OS.WINDOWS)
        void testSoloWindows() {
        }

        @Test
        @EnabledOnOs({OS.LINUX, OS.MAC})
        void testSoloLinuxMac() {
        }

        @Test
        @DisabledOnOs(OS.WINDOWS)
        void testNoWindows() {
        }
    }

    @Nested
    class JavaVersionTest{
        @Test
        @EnabledOnJre(JRE.JAVA_8)
        void testSoloJdk8() {
        }

        @Test
        @DisabledOnJre(JRE.JAVA_11)
        void noJDK11() {
        }
    }

    @Nested
    class SystemPropertiesTest{
        @Test
        void imprimirSystemProperties() {
            Properties properties = System.getProperties();
            System.out.println("\n\nLISTADO DE PROPERTIES: ");
            properties.forEach((k, v) -> System.out.println(k + " : " + v));
        }

        @Test
        @EnabledIfSystemProperty(named = "java.version", matches = "11.1.11")
        void testJavaVersion() {
        }

        @Test
        @EnabledIfSystemProperty(named = "user.name", matches = "MarianoVill")
        void testUsername() {
        }

        @Test
        @EnabledIfSystemProperty(named = "env", matches = "prd")
        void testDev() {
        }
    }

    @Nested
    class VariablesEntornoTest{

        @Test
        void imprimirVariablesEntorno() {
            Map<String, String> getenv = System.getenv();
            System.out.println("\n\nLISTADO DE VARIABLES DE ENTORNO: ");
            getenv.forEach((k, v) -> System.out.println(k + " = " + v));
        }

        @Test
        @EnabledIfEnvironmentVariable(named = "JAVA_HOME", matches = ".*jdk-11.0.11.9-hotspot:*")
        void testJavaHome() {
        }

        @Test
        @EnabledIfEnvironmentVariable(named = "NUMBER_OF_PROCESSORS", matches = "5")
        void testProcesadores() {
        }

        @Test
        @EnabledIfEnvironmentVariable(named = "ENVIRONMENT", matches = "prod")
        void testEnvProdDisabled() {
        }
    }

//***ASSUMPTIONS***
    //Uso de @assumeTrue
    @Test
    @DisplayName("Test Saldo Cuenta dev con el uso de Assumtions")
    void testSaldoCuentaDev() {
        boolean esDev = "dev".equals(System.getProperty("ENV"));
        assumeTrue(esDev);
        assertNotNull(cuenta.getSaldo());
        assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
        assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
    }

    //Uso de @assumingThat
    @Test
    @DisplayName("Test Saldo Cuenta dev 2 con el uso de Assumtions")
    void testSaldoCuentaDev2() {
        boolean esDev = "prd".equals(System.getProperty("env"));
        assumingThat(esDev, () -> {
            assertNotNull(cuenta.getSaldo());
            assertEquals(1000.12345, cuenta.getSaldo().doubleValue());
        });

        assertFalse(cuenta.getSaldo().compareTo(BigDecimal.ZERO) < 0);
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
    }

    @DisplayName("Probando Debito Cuenta Repetir!")
    @RepeatedTest(value = 5, name = "{displayName} - Repetición número {currentRepetition} de {totalRepetitions}")
    void testDebitoCuentaRepetir(RepetitionInfo info) {
        if(info.getCurrentRepetition() == 3){
            System.out.println("Estamos en la repetición " + info.getCurrentRepetition()    );
        }
        cuenta.credito(new BigDecimal(100));
        assertNotNull(cuenta.getSaldo());
        assertEquals(1100, cuenta.getSaldo().intValue());
        assertEquals("1100.12345", cuenta.getSaldo().toPlainString());
    }

    @Tag("param")
    @Nested
    class PruebasParametrizadasTest{

        @ParameterizedTest(name = "num {index} ejecutando con valor {0} - {argumentsWithNames}")
        @ValueSource(strings = {"100", "200", "300", "500", "700", "1000.12345"})
        void testDebitoCuentaValueSource(String monto) {
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @ParameterizedTest(name = "num {index} ejecutando con valor {0} - {argumentsWithNames}")
        @CsvSource({"1, 100", "2, 200", "3, 300", "4, 500", "5, 700", "6, 1000.12345"})
        void testDebitoCuentaCsvSource(String index, String monto) {
            System.out.println(index + " -> " + monto);
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @ParameterizedTest(name = "num {index} ejecutando con valor {0} - {argumentsWithNames}")
        @CsvFileSource(resources = "/data.csv")
        void testDebitoCuentaCsvFileSource(String monto) {
            cuenta.debito(new BigDecimal(monto));
            assertNotNull(cuenta.getSaldo());
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }

        @ParameterizedTest(name = "num {index} ejecutando con valor {0} - {argumentsWithNames}")
        @CsvSource({"200, 100, John, Andres", "250, 200, Pepe, Pepe", "300, 300, maria, Maria", "510, 500, Pepa, Pepa", "750, 700, Lucas, Luca", "1000.12345, 1000.12345, Algo, Algo"})
        void testDebitoCuentaCsvSource2(String saldo, String monto, String esperado, String actual) {
            System.out.println(saldo + " -> " + monto);
            cuenta.setSaldo(new BigDecimal(saldo));
            cuenta.debito(new BigDecimal(monto));
            cuenta.setPersona(actual);

            assertNotNull(cuenta.getSaldo());
            assertNotNull(cuenta.getPersona());
            assertEquals(esperado, actual);
            assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
        }
    }

    @Tag("param")
    @ParameterizedTest(name = "num {index} ejecutando con valor {0} - {argumentsWithNames}")
    @MethodSource("montoList")
    void testDebitoCuentaMethodSource(String monto) {
        cuenta.debito(new BigDecimal(monto));
        assertNotNull(cuenta.getSaldo());
        assertTrue(cuenta.getSaldo().compareTo(BigDecimal.ZERO) > 0);
    }

    static List<String> montoList(){
        return Arrays.asList("100", "200", "300", "500", "700", "1000.12345");
    }

    @Nested
    @Tag("timeout")
    class EjemploTimeout{
        //Por default, el tiempo está en segundos
        @Test
        @Timeout(2)
        void pruebasTimeout() throws InterruptedException {
            TimeUnit.SECONDS.sleep(1);
        }

        @Test
        @Timeout(value = 1000, unit = TimeUnit.MILLISECONDS)
        void pruebasTimeout2() throws InterruptedException {
            TimeUnit.MILLISECONDS.sleep(900);
        }

        @Test
        void pruebasTimeout3() {
            assertTimeout(Duration.ofSeconds(2), () -> {
                TimeUnit.MILLISECONDS.sleep(1000);
            });
        }
    }
}